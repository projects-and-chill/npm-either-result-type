
//------ For callers who receive a Result------

abstract class ResultAbst<U> {

  abstract readonly value: U

  abstract pipe<T>(func: (u:U)=> T|Result<unknown, T> ) : Success<T> | Failure<unknown>
  abstract pipeAsync<T>(func: (u:U)=> Promise<T|Result<unknown, T>> ) : any

  abstract isFailure()
  abstract isSuccess()
}



class Failure<F> extends ResultAbst<F> {

  readonly value: F;

  constructor(failValue: F) {
    super()
    this.value = failValue;
  }

  pipe<T>(fun: (f:any)=> T|Result<unknown,T> ) : this {
    return this
  }

  async resolve(){
    return this
  }

  async pipeAsync<T>(fun: (f: any) => Promise<T|Result<unknown,T>>): Promise<this> {
    return this
  }


  isFailure(): this is Failure<F> {
    return true
  }

  isSuccess(): this is Success<never> {
    return false
  }

}


class Success<S>  extends ResultAbst<S>{

  readonly value: S;


  constructor(successValue: S) {
    super()
    this.value = successValue;
  }

  pipe<T>(fun: (s:S)=> T|Result<unknown, T> ) : Success<T> | Failure<unknown> {
    const result = fun(this.value)
    if(result instanceof ResultAbst)
      return result
    return success(result)
  }

  async pipeAsync<T>(fun: (s:S)=> Promise<T|Result<unknown, T>> ) : Promise<Success<T> | Failure<unknown>> {
    const result = await fun(this.value)
    if(result instanceof ResultAbst)
      return result
    return success(result)
  }


  isFailure(): this is Failure<never> {
    return false;
  }

  isSuccess(): this is Success<S> {
    return true;
  }

}

//------- For called functionnalities who send a Result --------

const failure = <F>(failValue: F): Result<F,never> => {
  return new Failure(failValue);
};

const success = <S>(successValue: S): Result<never,S> => {
  return new Success<S>(successValue);
};



//------ For typing result output ------
type Result<F, S> = Failure<F> | Success<S>;


//------- Pipe functions --------

const asyncReducer = async  (prevPromise, currFn) => {
  const prevRes = await prevPromise
  let prevResponse : Success<unknown>
  if(prevRes instanceof Failure)
    return prevRes
  else if(!(prevRes instanceof Success))
    prevResponse = success(prevRes)
  else
    prevResponse = prevRes

  const res = await currFn(prevResponse.value)
  if(res instanceof ResultAbst)
    return res
  else
    return success(res)
}


const reducer = (prevRes, currFn: Function) : ResultAbst<unknown> => {
  let prevResponse : Success<unknown>
  if(prevRes instanceof Failure)
    return prevRes
  else if(!(prevRes instanceof Success))
    prevResponse = success(prevRes)
  else
    prevResponse = prevRes

  const res = currFn(prevResponse.value)
  if(res instanceof ResultAbst)
    return res
  else
    return success(res)
}

const pipeAsync = (...fns: Function[]) => (arg1: any) => fns.reduce(asyncReducer, arg1)
const pipe = (...fns: Function[])  => (arg1: any)  => fns.reduce(reducer, arg1)


export {pipe,pipeAsync,Result, success,Success, failure, Failure}